const Twit = require("twit");
const { prompt } = require('enquirer');
var clc = require("cli-color");

var banner = clc.xterm(202).bgXterm(0);
const question = [
	{
	  type: 'input',
	  name: "hashtag",
	  message: "Masukan Keyword",
	  warning: "Masukan Keyword.."
	}
  ];
  
const T = new Twit({
  consumer_key: "4xi5A5IzW5AAdF356mqImZuXd",
  consumer_secret: "4whzb4G9KiZnekUFW8vLeQZiDaczbMrt9VvJ1BOcXlbzLPuLfg",
  access_token: "1569445380349374466-wb0IRj7I07A2JIyJdUC0F7V46ko5xA",
  access_token_secret: "XfasYJRrpJH7LBE4pORhk5seCSmtiRKTW5LeKyRyR3d8Q",
});

var delayBetweenSearches = 60 * 60 * 1000;

function retweet(idOfTweet) {
  T.post(
    "https://api.twitter.com/1.1/statuses/retweet/:id.json",
    { id: idOfTweet },
    (err) => {
      if (err) {
        console.log("There was an error retweeting this");
      } else {
        console.log("Successfully retweeted: ", idOfTweet);
      }
    }
  );
}
function likeTweet(idOfTweet) {
  T.post(
    "https://api.twitter.com/1.1/favorites/create.json",
    { id: idOfTweet },
    (err) => {
      if (err) {
        console.log("There was an error liking this");
      } else {
        console.log("Successfully liked: ", idOfTweet);
      }
    }
  );
}

function Comment(idOfTweet) {
  T.post(
	"https://api.twitter.com/1.1/statuses/update.json",
	{ id: idOfTweet },
	(err) => {
	  if (err) {
		console.log("There was an error commenting this");
	  } else {
		console.log("Successfully commented: ", idOfTweet);
	  }
	}
  );
}

const delay = (ms) => new Promise((res) => setTimeout(res, ms));


async function getTweets() {
	let result = await prompt(question);
  let current = Date.now();
  let numOfMinutesBeforeCurrent = 30;
  let numOfMillisecBeforeCUrrent =
    current - numOfMinutesBeforeCurrent * 60 * 1000;

  let startTimeISO = new Date(numOfMillisecBeforeCUrrent);

  let searchParams = {
    query: "#bjorka",
    max_results: 12,
    start_time: startTimeISO,
  };

  T.get(
    "https://api.twitter.com/2/tweets/search/recent",
    searchParams,
    (err, tweetData, response) => {
      if (err) {
        console.log({ err });
        console.log("Error getting tweets");
      } else {
        console.log(tweetData.data[0]);
        //  const tweets = new Set(tweetData.data.map(tweet => tweet.id));
        //tweets.forEach(tweet =>{ retweet(tweet)
        for (let i = 0; i < tweetData.data.length; i++) {
          retweet(tweetData.data[i].id);
          likeTweet(tweetData.data[i].id);
		  Comment(tweetData.data[i].id);
		  
        }
        //	})
      }
    }
  );
}

console.log("Twitter Auto Retweet Bot");
getTweets();
setInterval(getTweets, delayBetweenSearches);
